export declare type ChartType = 'box' | 'sensor';
export enum ChartTypes {
    BOX = 'box',
    SENSOR = 'sensor'
}