import axios, { AxiosError, AxiosInstance, AxiosResponse } from 'axios';
import { resolve } from 'path';

const axiosInstance: AxiosInstance = axios.create({
    baseURL: 'http://192.168.1.248:8000/api',
    headers: {
        'Content-Type': 'application/json',
    },
})

export const getInfluxToken = (): Promise<string> => {
    return new Promise((resolve, reject) => {
        axiosInstance.get('influx-token').then((data: AxiosResponse) => {
            console.log(data.data)
            resolve(data.data.token);
        }).catch((error: AxiosError) => {
            console.error(error);
            reject();
        })
    })
}