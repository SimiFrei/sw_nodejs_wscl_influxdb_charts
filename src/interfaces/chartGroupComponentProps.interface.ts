import { InfluxDbClient } from "../influxdb-client/influxdb-client";
import { IChartConfig } from "./config.interface";

export interface IChartGroupComponentProps {
    config: IChartConfig;
    startDate: Date;
    endDate: Date;
    token: string;
}