export interface IMeasurement {
    time: string;
    value: number | null;
}

export interface IBoxSensorMeasurement {
    sensorType: string; 
    sensorMeasurements: IMeasurement[];
}

export interface IBoxMeasurement {
    boxId: string;
    boxMeasurements: IBoxSensorMeasurement[];
}

export interface ISensorByBoxMeasurement {
    boxId: string;
    measurements: IMeasurement[];
}