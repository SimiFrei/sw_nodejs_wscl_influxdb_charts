import { ChartData, ChartOptions } from "chart.js";

export interface IChartComponentProps {
    data: ChartData<"line">;
    options: ChartOptions;
}