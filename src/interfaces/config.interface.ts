import { ChartOptions } from "chart.js";
import { ChartType } from "../_helpers/chartType";

export interface ISensorConfig {
    name: string;
    axis: string;
    borderColor?: string;
    /**
     * needed when multiple sensors should be displayed in the same chart
     * only for type = ChartType.SENSOR has no affect on other types
     */
    chart?: string;
    axisName?: string;
}

export interface IChartConfig {
    name: string;
    type: ChartType;
    sensors: ISensorConfig[];
    options: ChartOptions<"line">;
    colorsPerBox?: {boxId: string, color: string}[]
}