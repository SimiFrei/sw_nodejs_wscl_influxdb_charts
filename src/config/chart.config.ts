import { format } from "date-fns"
import { IChartConfig } from "../interfaces/config.interface"
import { ChartTypes } from "../_helpers/chartType"

export const ChartConfig: IChartConfig[] = [
    {
        name: "box",
        type: ChartTypes.BOX,
        sensors: [
            {
                name: "hum",
                axis: "y1",
                borderColor: 'rgb(75, 192, 192)'
            },
            {
                name: "temp",
                axis: "y",
                borderColor: 'rgb(255, 192, 192)'
            },
            {
                name: "power",
                axis: "y2",
                borderColor: 'rgb(255, 0, 0)'
            }
        ],
        options: {
            responsive: true,
            scales: {
                x: {
                    type: "time",
                    time: {
                        parser: (value: any) => new Date(value).getTime(),
                        tooltipFormat: 'yy-MM-dd HH:mm',
                        unit: 'day', 
                    },
                    ticks: {
                        source: 'data',
                        autoSkip: true,
                        maxTicksLimit: 6,
                        callback: (value: any, index: number, values: any[]) => {
                            for (let i = 0; i < index; i++) {
                                if (format(value, 'yy-MM-dd') === format(values[i].value, 'yy-MM-dd')) {
                                    return ''
                                }
                            }
                            return format(value, 'yy-MM-dd HH:mm')
                        },
                    },
                },
                y: {
                    type: 'linear',
                    display: true,
                    position: 'left',
                    title: {
                        display: true,
                        text: 'Temperature [°C]'
                    }
                },
                y1: {
                    type: 'linear',
                    display: true,
                    position: 'left',
                    grid: {
                        drawOnChartArea: false, // only want the grid lines for one axis to show up
                    },
                    title: {
                        display: true,
                        text: 'Humidity [%]'
                    }
                },
                y2: {
                    type: 'linear',
                    display: true,
                    position: 'right',
                    grid: {
                        drawOnChartArea: false, // only want the grid lines for one axis to show up
                    },
                    title: {
                        display: true,
                        text: 'Power [W]'
                    }
                },
            },
            plugins: {
                title: {
                    display: true,
                    text: 'Box Id '
                },
                tooltip: {
                    mode: 'nearest',
                    axis: 'xy',
                    intersect: false, // false --> tooltip appears everytime clicked on the graph on the nearest point
                }
            },
        }
    },
    {
        name: "sensors",
        type: ChartTypes.SENSOR,
        colorsPerBox: [
            { boxId: '0', color: 'rgb(75, 192, 192)'},
            { boxId: '1', color: 'rgb(255, 192, 192)'},
            { boxId: '2', color: 'rgb(0, 255, 192)'},
            { boxId: '3', color: 'rgb(0, 0, 255)'},
        ],
        sensors: [
            {
                name: "temp",
                axis: "y",
                axisName: "Temperature [°C]" 
            },
            {
                name: "hum",
                axis: "y",
                axisName: "Humidity [%]" 
            },
            {
                name: "current",
                axis: "y1",
                axisName: "Current [A]" ,
                chart: "abc"
            },
            {
                name: "power",
                axis: "y",
                axisName: "Power [W]" ,
                chart: "abc"
            }
        ],
        options: {
            responsive: true,
            scales: {
                x: {
                    type: "time",
                    time: {
                        parser: (value: any) => new Date(value).getTime(),
                        tooltipFormat: 'yy-MM-dd HH:mm',
                        unit: 'day', 
                    },
                    ticks: {
                        source: 'data',
                        autoSkip: true,
                        maxTicksLimit: 6,
                        callback: (value: any, index: number, values: any[]) => {
                            for (let i = 0; i < index; i++) {
                                if (format(value, 'yy-MM-dd') === format(values[i].value, 'yy-MM-dd')) {
                                    return ''
                                }
                            }
                            return format(value, 'yy-MM-dd HH:mm')
                        },
                    },
                },
                y: {
                    type: 'linear',
                    display: true,
                    position: 'left',
                    title: {
                        display: true,
                        text: 'Temperature [°C]'
                    }
                },
            },
            plugins: {
                title: {
                    display: true,
                    text: 'Box Id '
                },
                tooltip: {
                    mode: 'nearest',
                    axis: 'xy',
                    intersect: false, // false --> tooltip appears everytime clicked on the graph on the nearest point
                }
            },
        }

    }
]