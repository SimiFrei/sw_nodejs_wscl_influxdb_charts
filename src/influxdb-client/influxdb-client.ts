import { FluxTableColumn, FluxTableMetaData, InfluxDB, QueryApi, stringToLines } from "@influxdata/influxdb-client";
import { boxIdTag, bucketName, sensorTypeTag } from "../_helpers/influxdb.constants";
import { IBoxMeasurement, IMeasurement, IBoxSensorMeasurement, ISensorByBoxMeasurement } from "../interfaces/measurementQueries.interface";
import { ChartData, ChartDataset, ChartOptions, Point } from "chart.js";
import { IChartConfig, ISensorConfig } from "../interfaces/config.interface";
import { ChartConfig } from "../config/chart.config";
import { ChartTypes } from "../_helpers/chartType";

export class InfluxDbClient {
    private influxDbClient: InfluxDB;
    private queryClient: QueryApi;
    constructor(url: string, token: string, organisation: string) {
        this.influxDbClient = new InfluxDB({ url, token });
        this.queryClient = this.influxDbClient.getQueryApi(organisation);
    }

    public async getMeasurements(config: IChartConfig, start: Date = new Date(), end: Date = new Date()): Promise<{ id: string, chartData: ChartData<"line">, options: ChartOptions<"line"> }[]> {
        console.log('get measurement config', config);
        if (start === end) {
            start.setMonth(start.getMonth() - 3);
        }
        switch (config.type) {
            case ChartTypes.BOX:
                return this.getMeasurementsPerBox(config, start, end);
            case ChartTypes.SENSOR:
                return this.getAllMeasurementsPerSensor(config,start,end);
        }
        return this.getMeasurementsPerBox(config, start, end);

    }

    public async getMeasurementsPerBox(config: IChartConfig, start: Date, end: Date): Promise<{ id: string, chartData: ChartData<"line">, options: ChartOptions<"line"> }[]> {
        const boxIds: string[] = await this.getBoxes(start, end);
        console.log(boxIds);
        const chartData: { id: string, chartData: ChartData<"line">, options: ChartOptions<"line"> }[] = [];
        const boxMeasurements: IBoxMeasurement[] = await Promise.all(boxIds.map(async (boxId: string) => {
            const sensorTypes: string[] = await this.getSensorTypesPerBox(boxId, start, end)
            const boxMeasurements: IBoxSensorMeasurement[] = await Promise.all(sensorTypes.map(async (sensor: string) => {
                const sensorMeasurements: IMeasurement[] = await this.getMeasurementsPerSensor(boxId, sensor, start, end);
                return { sensorType: sensor, sensorMeasurements };
            }))
            return { boxId, boxMeasurements };
        }))
        boxMeasurements.forEach((boxMeasurement: IBoxMeasurement) => {
            const boxTimes: string[][] = boxMeasurement.boxMeasurements.map((value: IBoxSensorMeasurement) => {
                const times: string[] = value.sensorMeasurements.map((value: IMeasurement) => { return value.time })
                return times
            })
            const boxTimesFlatUnique: string[] = Array.from(new Set(boxTimes.flat()).values())
            const boxTimesSorted: string[] = boxTimesFlatUnique.sort((a, b) => new Date(a).getTime() - new Date(b).getTime());
            boxTimesSorted.forEach((time: string) => {
                boxMeasurement.boxMeasurements.forEach((value: IBoxSensorMeasurement) => {
                    if (!value.sensorMeasurements.map((meas) => { return meas.time }).includes(time)) {
                        value.sensorMeasurements.push({ time: time, value: null })
                    }
                })
            })
            chartData.push({
                id: boxMeasurement.boxId,
                chartData: {
                    labels: boxTimesFlatUnique,
                    datasets: this.getChartDatasetBox(boxMeasurement, config)
                },
                options: config.options
            })
        })
        return chartData;
    }

    public async getAllMeasurementsPerSensor(config: IChartConfig, start: Date, end: Date): Promise<{ id: string, chartData: ChartData<"line">, options: ChartOptions<"line"> }[]> {
        const chartData: { id: string, chartData: ChartData<"line">, options: ChartOptions<"line"> }[] = await Promise.all(config.sensors.map(async (sensor: ISensorConfig) => {
            const sensorByBoxes: ISensorByBoxMeasurement[] = await this.getAllMeasuraementsPerSensorType(sensor.name, start, end);
            const times: string[][] = sensorByBoxes.map((values: ISensorByBoxMeasurement) => {
                return values.measurements.map((measurement: IMeasurement) => {
                    return measurement.time;
                })
            })
            const timesUnique: string[] = Array.from(new Set(times.flat()).values());
            const timeSorted: string[] = timesUnique.sort((a, b) => new Date(a).getTime() - new Date(b).getTime());
            timeSorted.forEach((time: string) => {
                sensorByBoxes.forEach((value: ISensorByBoxMeasurement) => {
                    if (!value.measurements.map((meas: IMeasurement) => { return meas.time }).includes(time)) {
                        value.measurements.push({ time: time, value: null });
                    }
                })
            })
            console.log('sensor by boxes', sensorByBoxes)
            console.log(sensor.name, this.getChartDatasetSensor(sensorByBoxes, sensor.name, config))
            return {
                id: sensor.name,
                chartData: {
                    labels: timesUnique,
                    datasets: this.getChartDatasetSensor(sensorByBoxes, sensor.name, config)
                },
                options: {
                    ...config.options,
                    // scales.y.title: title: sensor.axisName ? sensor.axisName : ''
                    scales: {
                        ...config.options.scales,
                        y: {
                            ...config.options.scales?.y,
                            title: {
                                ...config.options.scales?.y?.title,
                                text: sensor.axisName ? sensor.axisName : ''
                            } 
                        }
                    }
                }
            }
        }))
        console.log('chartdata', chartData);
        const sameCharts: string[] = Array.from(new Set(config.sensors.filter((value: ISensorConfig) => value.chart)
            .map((value: ISensorConfig) => {
                return value.chart!
            })))
        console.log('sameCharts', sameCharts);
        sameCharts.forEach((chart: string) => {
            const sensorNames: string[] = config.sensors.filter((value: ISensorConfig) => value.chart === chart)
                .map((value: ISensorConfig) => value.name);
            const chartDatas: { id: string, chartData: ChartData<"line">, options: ChartOptions<"line"> }[] = []
            console.log('sensor names of samecharts', sensorNames);
            console.log(chartData);
            sensorNames.forEach((sensor: string) => {
            //     // chartDatas.push(chartData.find((value) => value.id === sensor)!);
                const idx = chartData.findIndex((value) => value.id === sensor);
                console.log(idx, sensor);
                chartDatas.push(chartData[idx]);
            //     delete chartData[idx];
            })
            chartDatas.forEach((elem) => {
                const idx = chartData.indexOf(elem);
                if(idx !== -1){
                    chartData.splice(idx,1);
                }
                
            })
            console.log(chartData);
            chartDatas.slice(1).forEach((chart) => {
                chart.chartData.datasets.forEach((data) => {
                    data.label += ' '+chart.id
                    data.borderColor = this.convertRgbString(data.borderColor ? data.borderColor as string : '');
                    chartDatas[0].chartData.datasets.push(data);
                    if(chart.options.scales?.y && chartDatas[0].options.scales) {
                        chartDatas[0].options.scales.y1= { ...chart.options.scales?.y!}
                    }
                })
            })
            console.log('chartDatas', chartDatas);
            chartData.push(chartDatas[0]);

        })
        console.log('sensor chart data', chartData);
        return chartData;

    }

    private convertRgbString(rgbString: string): string {
        console.log('rgbstring',rgbString)
        const match = rgbString.match(new RegExp('((\\d+),\\s*(\\d+),\\s*(\\d+))', ''))
        if(!match){
            return rgbString;
        }
        const[,,red,green,blue] = match.map(Number);
        // const decRed = red.toString(10)//.padStart(2, '0');
        // const decGreen = green.toString(10)//.padStart(2, '0');
        // const decBlue = blue.toString(10)//.padStart(2, '0');
        console.log(red,green,blue);
        return `rgb(${255-red},${255-green},${255-blue})`;
    }
    private getChartDatasetBox(boxMeasurements: IBoxMeasurement, chartConfig: IChartConfig): ChartDataset<"line", (number | Point | null)[]>[] {
        const datasets: ChartDataset<"line", (number | Point | null)[]>[] = [];
        boxMeasurements.boxMeasurements.forEach((meas: IBoxSensorMeasurement) => {
            if (chartConfig.sensors.some((value) => value.name === meas.sensorType)) {
                const data: (number | null)[] = meas.sensorMeasurements.sort((a, b) => new Date(a.time).getTime() - new Date(b.time).getTime()).map((value) => { return value.value });
                datasets.push({
                    data: data,
                    label: meas.sensorType,
                    spanGaps: true,
                    yAxisID: chartConfig.sensors.filter((v) => v.name === meas.sensorType)[0].axis,
                    fill: false,
                    borderColor: chartConfig.sensors.filter((v) => v.name === meas.sensorType)[0].borderColor,
                    tension: 0.1,
                })
            }
        })
        return datasets;
    }

    private getChartDatasetSensor(sensorMeasurements: ISensorByBoxMeasurement[], sensor: string, chartConfig: IChartConfig): ChartDataset<"line", (number | Point | null)[]>[] {
        const datasets: ChartDataset<"line", (number | Point | null)[]>[] = [];
        sensorMeasurements.forEach((sensorMeasurement: ISensorByBoxMeasurement) => {
            const data: (number | null)[] = sensorMeasurement.measurements.sort((a, b) => new Date(a.time).getTime() - new Date(b.time).getTime())
                .map((value: IMeasurement) => { return value.value });
            datasets.push({
                data: data,
                label: 'box ' + sensorMeasurement.boxId,
                spanGaps: true,
                yAxisID: chartConfig.sensors.filter((value: ISensorConfig) => value.name === sensor)[0].axis,
                fill: false,
                tension: 0.1,
                borderColor: chartConfig.colorsPerBox ? chartConfig.colorsPerBox.filter((elem: { boxId: string, color: string }) => elem.boxId === sensorMeasurement.boxId)[0].color : 'rgb(255, 255, 255)'
            })
        })
        return datasets;
    }

    private async getBoxes(start: Date, end: Date): Promise<string[]> {
        console.log('getBoxes', start.toISOString(), end.toISOString());
        const query: string = `from(bucket: "${bucketName}")
        |> range(start: ${start.toISOString()}, stop: ${end.toISOString()})
        |> distinct(column: "${boxIdTag}")
        |> keep(columns: ["${boxIdTag}"])`
        // const query: string = `import "influxdata/influxdb/v1"
        // v1.tagValues(bucket: "${bucketName}", tag: "${boxIdTag}")
        // |> keep(columns: ["_value"])
        // |> rename(columns: {_value: "box_ids"})`;
        // |> range(start: ${start}, stop: ${end})


        const uniqueBoxes: Set<string> = new Set(await this.queryClient.collectRows<string>(query, (values: string[], table: FluxTableMetaData) => {
            const x = table.columns.findIndex((value: FluxTableColumn) => value.label === `${boxIdTag}`)
            return values[x];
        }))
        return Array.from(uniqueBoxes.values());
    }

    private async getSensorTypes(start: Date, end: Date): Promise<string[]> {

        const query: string = `from(bucket: "${bucketName}")
        |> range(start: ${start.toISOString()}, stop: ${end.toISOString()})
        |> distinct(column: "${sensorTypeTag}")
        |> keep(columns: ["${sensorTypeTag}"])`

        const uniqueBoxes: Set<string> = new Set(await this.queryClient.collectRows<string>(query, (values: string[], table: FluxTableMetaData) => {
            const x = table.columns.findIndex((value: FluxTableColumn) => value.label === `${sensorTypeTag}`)
            return values[x];
        }))
        return Array.from(uniqueBoxes.values());
    }

    private async getSensorTypesPerBox(boxId: string, start: Date, end: Date): Promise<string[]> {
        const query: string = `from(bucket: "${bucketName}")
        |> range(start: ${start.toISOString()}, stop: ${end.toISOString()})
        |> filter(fn: (r) => r["${boxIdTag}"] == "${boxId}")
        |> distinct(column: "_field")
        |> keep(columns: ["_field"])
        |> rename(columns: {_field: "sensorType"})`
        const sensorTypePerBox: Set<string> = new Set(await this.queryClient.collectRows<string>(query, (values: string[], table: FluxTableMetaData) => {
            const x = table.columns.findIndex((value: FluxTableColumn) => value.label === 'sensorType')
            return values[x];
        }))
        return Array.from(sensorTypePerBox.values());
    }

    private async getMeasurementsPerSensor(boxId: string, sensorType: string, start: Date, end: Date): Promise<IMeasurement[]> {
        const query: string = `from(bucket: "${bucketName}")
        |> range(start: ${start.toISOString()}, stop: ${end.toISOString()})
        |> filter(fn: (r) => r["${sensorTypeTag}"] == "${sensorType}")
        |> filter(fn: (r) => r["${boxIdTag}"] == "${boxId}")
        |> keep(columns: ["_value", "_time"])`

        return await this.queryClient.collectRows<{ time: string, value: number }>(query, (values: string[], table: FluxTableMetaData) => {
            const x = table.columns.findIndex((value: FluxTableColumn) => value.label === '_value')
            const y = table.columns.findIndex((value: FluxTableColumn) => value.label === '_time')
            return { time: values[y], value: Number(values[x]) };
        });
    }

    private async getAllMeasuraementsPerSensorType(sensorType: string, start: Date, end: Date): Promise<ISensorByBoxMeasurement[]> {
        const query: string = `from(bucket: "${bucketName}")
        |> range(start: ${start.toISOString()}, stop: ${end.toISOString()})
        |> filter(fn: (r) => r["${sensorTypeTag}"] == "${sensorType}")
        |> keep(columns: ["_value","_time","${boxIdTag}"])`
        const measurements: { time: string, value: number, boxId: string }[] = await this.queryClient.collectRows<{ time: string, value: number; boxId: string }>(query, (values: string[], table: FluxTableMetaData) => {
            const x = table.columns.findIndex((value: FluxTableColumn) => value.label === '_value')
            const y = table.columns.findIndex((value: FluxTableColumn) => value.label === '_time')
            const z = table.columns.findIndex((value: FluxTableColumn) => value.label === `${boxIdTag}`);
            return { time: values[y], value: Number(values[x]), boxId: values[z] };
        })

        const uniqueBoxes: string[] = Array.from(
            new Set(
                measurements.map((measurement: { time: string, value: number, boxId: string }) => { return measurement.boxId }
                )
            )
        )
        const sensorByBoxMeas: ISensorByBoxMeasurement[] = []
        uniqueBoxes.forEach((box: string) => {
            sensorByBoxMeas.push({
                boxId: box,
                measurements: measurements.filter((measurement) => measurement.boxId === box).map((elem) => { return { time: elem.time, value: elem.value } })
            })
        })

        return sensorByBoxMeas;
    }




}