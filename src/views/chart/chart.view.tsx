import { AppBar, Box, CssBaseline, Divider, Drawer, IconButton, List, ListItem, ListItemButton, ListItemText, Toolbar, styled, useTheme } from "@mui/material";
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import { useState } from "react";
import { ChartConfig } from "../../config/chart.config";
import { IChartConfig } from "../../interfaces/config.interface";
import ChartGroupComponent from "../../components/chartGroup/chartGroup.component"
import {InfluxDbClient} from "../../influxdb-client/influxdb-client"
import { grid } from "@mui/system";
import DatePickerComponent from "../../components/datepicker/datepicker.component";
import dayjs, { Dayjs } from "dayjs";
import { getInfluxToken } from "../../_helpers/influxtoken";
import { useQuery } from "react-query";
const ChartView: React.FC = () => {

    // const client = new InfluxDbClient('https://iot.wscl.duckdns.org/',
    // 'GQY9RlfZcqwuVY3uan1wpJh73hB4__FnLAmvu98w7fZL_UZe132iklbISO4i8oAHUQoLz87PJ_53j-KwRz0jYg==',
    // 'WSCL');
    const drawerWidth = 100;

    const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })<{
        open?: boolean;
    }>(({ theme, open }) => ({
        flexGrow: 1,
        padding: theme.spacing(3),
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: `-${drawerWidth}px`,
        ...(open && {
            transition: theme.transitions.create('margin', {
                easing: theme.transitions.easing.easeOut,
                duration: theme.transitions.duration.enteringScreen,
            }),
            marginLeft: 0,
        }),
    }));

    interface AppBarProps extends MuiAppBarProps {
        open?: boolean;
    }

    const AppBar = styled(MuiAppBar, {
        shouldForwardProp: (prop) => prop !== 'open',
    })<AppBarProps>(({ theme, open }) => ({
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        ...(open && {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: `${drawerWidth}px`,
            transition: theme.transitions.create(['margin', 'width'], {
                easing: theme.transitions.easing.easeOut,
                duration: theme.transitions.duration.enteringScreen,
            }),
        }),
    }));

    const DrawerHeader = styled('div')(({ theme }) => ({
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    }));

    const theme = useTheme();
    const [open, setOpen] = useState<boolean>(false);
    const [chartConfig, setChartConfig] = useState<IChartConfig>(ChartConfig[0])
    // const [token, setToken] = useState<string>('');

    const { data: token, isError: isError, isLoading: isLoading } = useQuery<string>('getToken', getInfluxToken)

    console.log(token);
    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const handleListItemClicked = (config: IChartConfig) => {
        setChartConfig(config);
    }

    const [startDate, setStartDate] = useState<Dayjs>(dayjs().subtract(2, "weeks"));
    const [endDate, setEndDate] = useState<Dayjs>(dayjs());

    const handleStartDateChange = (newDate: Dayjs): void => {
            setStartDate(newDate);
            console.log(startDate, endDate);
    }

    const handleEndDateChange = (newDate: Dayjs): void => {
            setEndDate(newDate);
    }
    console.log(startDate, endDate);
    return (
        <Box sx={{ display: 'flex' }}>
            <CssBaseline />
            <AppBar position="fixed">
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        sx={{ mr: 2, ...(open && { display: 'none' }) }}>
                        <MenuIcon />
                    </IconButton>
                </Toolbar>
            </AppBar>
            <Drawer
                sx={{
                    width: `50 vw`,
                    flexShrink: 0,
                    '& .MuiDrawer-paper': {
                        width: drawerWidth,
                        boxSizing: 'border-box',
                    },
                }}
                variant="persistent"
                anchor="left"
                open={open}>
                <DrawerHeader>
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                </DrawerHeader>
                <Divider />
                <List>
                    {ChartConfig.map((conf, index) => (
                        <ListItem key={index} disablePadding>
                            <ListItemButton onClick={() => handleListItemClicked(conf)}>
                                <ListItemText primary={conf.name} />
                            </ListItemButton>
                        </ListItem>
                    ))}
                </List>
            </Drawer>
            <Main open={open}>
                <DrawerHeader />
                <DatePickerComponent startDate={startDate} endDate={endDate} onStartDateChange={handleStartDateChange} onEndDateChange={handleEndDateChange}/>
                <div style={{ marginLeft: '20vw', marginRight: '15vw'}}>
                {token !== undefined && !isError && !isLoading ? <ChartGroupComponent config={chartConfig} startDate={new Date(startDate.toISOString())} endDate={new Date(endDate.toISOString())} token={token} /> : <div>load token</div>}
                
                </div>
            </Main>
        </Box>
    )
}

export default ChartView;