import React from 'react';
import logo from './logo.svg';
import './App.css';
import { InfluxDbClient } from './influxdb-client/influxdb-client';
import ChartGroupComponent from './components/chartGroup/chartGroup.component';
import { ChartConfig } from './config/chart.config';
import ChartView from './views/chart/chart.view';

function App() {

  const client = new InfluxDbClient('https://iot.wscl.duckdns.org/',
    'GQY9RlfZcqwuVY3uan1wpJh73hB4__FnLAmvu98w7fZL_UZe132iklbISO4i8oAHUQoLz87PJ_53j-KwRz0jYg==',
    'WSCL');
  return (
    <div className="App">
      <header>
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
      </header>
      <ChartView></ChartView>
      {/* <ChartGroupComponent config={ChartConfig[0]} client={client}/> */}
    </div>
  );
}

export default App;
