import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import dayjs, { Dayjs } from "dayjs";
import { ChangeEvent, useState } from "react";

export interface IDatePickerComponentProps {
    startDate: Dayjs;
    endDate: Dayjs;
    onStartDateChange: (newStartDate: Dayjs) => void;
    onEndDateChange: (newEndDate: Dayjs) => void;
}

const DatePickerComponent: React.FC<IDatePickerComponentProps> = ({startDate, endDate, onStartDateChange, onEndDateChange}) => {
    // const [startDate, setStartDate] = useState(dayjs().subtract(1, "months"));
    // const [endDate, setEndDate] = useState(dayjs());

    const handleStartDateChange = (newDate: Dayjs | null): void => {
        if(newDate){
            // setStartDate(newDate);
            onStartDateChange(newDate);
            console.log('abcd', newDate);
        }
    }

    const handleEndDateChange = (newDate: Dayjs | null): void => {
        if(newDate){
            // setEndDate(newDate);
            onEndDateChange(newDate);
        }
    }

    return (
        <LocalizationProvider dateAdapter={AdapterDayjs}>
            {/* <div style={{width: '60vw', placeItems: "center"}}> */}
                <DatePicker label="Start Date" value={startDate} onAccept={handleStartDateChange}/>
                <DatePicker label="End Date" value={endDate} onAccept={handleEndDateChange} />
            {/* </div> */}
        </LocalizationProvider>
    )
}

export default DatePickerComponent;