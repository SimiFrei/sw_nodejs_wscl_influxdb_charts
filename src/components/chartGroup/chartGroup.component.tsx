import { ChartData, ChartOptions } from "chart.js";
import { IChartGroupComponentProps } from "../../interfaces/chartGroupComponentProps.interface";
import { useQuery } from "react-query";
import { InfluxDbClient } from "../../influxdb-client/influxdb-client";
import ChartComponent from "../chart/chart.component";
import { getInfluxToken } from "../../_helpers/influxtoken";

const ChartGroupComponent: React.FC<IChartGroupComponentProps> = (config) => {
    const client = new InfluxDbClient('https://iot.wscl.duckdns.org/',
        config.token,
        'WSCL');
    // const {data: influxPayload, isError, isLoading} = useQuery<{token: string}(['getInfluxToken'], )
    const { data: chartDatas, isError: isError, isLoading: isLoading } = useQuery<{ id: string, chartData: ChartData<"line">, options: ChartOptions<"line"> }[]>(['getChartData', config],
        () => client.getMeasurements(config.config, config.startDate, config.endDate))
    if (isError) {
        return <div>ERROR!</div>
    } else if (isLoading) {
        console.log('loading')
        return <div>LOADING!</div>
    } else if (chartDatas) {
        return (
            // <div className="container">
            <div>
                {chartDatas?.map((chartData: { id: string, chartData: ChartData<"line">, options: ChartOptions<"line"> }) => {
                    const chartOptions: ChartOptions = {
                        // ...config.config.options,
                        ...chartData.options,
                        plugins: {
                            title: {
                                text: config.config.name + chartData.id,
                                display: true
                            }
                        }
                    };
                    return (//<div className="item">
                        <ChartComponent data={chartData.chartData} options={chartOptions} key={chartData.id} />
                    )
                    //</div>)
                })}
            </div>
        )
    } else {
        return <div>test</div>
    }
}

export default ChartGroupComponent;