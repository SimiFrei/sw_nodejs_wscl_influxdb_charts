import {CategoryScale, Chart, Legend, LineElement, LinearScale, PointElement, TimeScale, Title, Tooltip} from "chart.js";
import { IChartComponentProps } from "../../interfaces/chartComponentProps.interface";
import { Line } from "react-chartjs-2";
import "chartjs-adapter-date-fns";

Chart.register(LinearScale);
Chart.register(CategoryScale);
Chart.register(PointElement);
Chart.register(LineElement);
Chart.register(Title);
Chart.register(Legend);
Chart.register(Tooltip);
Chart.register(TimeScale)

const ChartComponent: React.FC<IChartComponentProps> = ({data, options}) => {

    return <Line data={data} options={options} />
}

export default ChartComponent;